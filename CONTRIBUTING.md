## How to contribute

Since nobdy wants broken installers on Lutris we advice to follow this smiple  
contribution guide.

If you write an instaler test it localy, do changes, test it again and so on.  
Never work with draft installers on lutris.net since you can not be sure if  
someone else is working on an installer while you do too.

Our advice is, to make sure your installer runs localy and after you found it  
working publish it on lutris.net. But make double sure that the "live" version  
of your installer works too.  
For this save it as draft installer reload the edit intaller page and hit  
test installer.  
If everything worked feel free to hit the submit button.