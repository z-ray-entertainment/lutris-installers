## About this project

In the past we encounter multiple cases of broken Lutris installers on
lutris.net which were once working.
Because there is no verion history on the webpage it was difficult for us to find
out what changed and to properly fix the affected installer.

Also this is repository is used to track issues with some Lutris installers or
installers we're currently up to create but not yet submitted to lutris.net.

## Is this an official repository?

No, we (Z-Ray Entertainment) and also this repository is not maintained nor
related to Lutris.net or any of its official development teams.
Furthermore, all installers provided here are for local testing only and not
accessible via the Lutris client nor the Webpage.

## Do you maintain all Lutris installers here?

We do not officially work for or together with the Team behind Lutris but
generally we want to maintain as many installers as possible to make Lutris an
even better gaming platform and support it with as many working installers as
possible.

## Can I submit issues?

Yes you can submit issues to any installer on Lutris.net even if you can not find
it in this repository.
But also it depends on the game and if we own it to properly test the installer.

If you do not want to create a GitLab account you can email us at:
[support@z-ray.de](mailto:support@z-ray.de)
and we'll create an issue here where you can look up the current progress.
Your contact email will not be exposed in any ticket nor any other personal
data.

## Can I contribute?

You can request access to this repository at any time, clone the source, fork it,
submit issues or provide installers you've worked on and/or need some assistant
with.

If you want to contribute to this project by developing installers you need to
follow a very short and simple guide. For this follow the contribution guide.

## Can I support you?

Basically yes, but since we're not officially working with the Lutris Team
together we suggest supporting the Lutris Team rather than us.

## I want to make my own installer or need some help, can I ask you?

Yes and it makes no difference if you participate in this project or not.
Also since we claim to have a decent experience in writing and testing installers
we'd like to help as much as we can. If you have a GitLab account you can also
create an issue for requesting help or contact us via [support@z-ray.de](mailto:support@z-ray.de)