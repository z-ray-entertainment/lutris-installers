####################### HOW TO USE: ###########################
# Place this file inside your Star Wars the Force Unleashed 2 #
# folder Game/Disc/FMV relative to Win32.                     #
# And execute it simply via sh ./swtfu2_convert_movies.sh     #
# This script will backup all original ASF-Videos in          #
# Win32/backup and convert all of them to Win32/converted with#
# all audio tracks included and move them back to FMV/Win32.  #
# This allows Wine/Proton to use linux native video encoders  #
# to display the movies.                                      #
#                                                             #
# ATTENTION: This process may take a while depending on your  #
# Hardware.                                                   #
#                                                             #
# Dependencies:                                               #
# - ffmpeg                                                    #
#                                                             #
# Videos works only whith Proton GE 6.0 and newer in SWTFU2   #
###############################################################

#!/bin/bash

cd ./Win32
backup_dir=./backup
converted_dir=./converted
video_format=mp4
mkdir $converted_dir
mkdir $backup_dir

echo "==== Backing up original files ===="

for f in ./*
do
    if [[ -d $f ]]
    then
        echo "==== Skip folder $f ===="
    else
        mv $f $backup_dir/$f
    fi
done

echo "==== Start converting files, this may take a while ===="
cd $backup_dir
for f in ./*
do
 echo "==== Convert file: $f ===="
    ffmpeg -i ./$f -map 0:v -map 0:a -c:v libx264 -preset fast -crf 18 -c:a aac -b:a 256k ../$converted_dir/$f.$video_format
    #ffmpeg -i ./$f -c:v libx264 -preset fast -crf 18 -c:a aac -b:a 256k ../$converted_dir/$f.$video_format
    mv ../$converted_dir/$f.$video_format ../$f
done
